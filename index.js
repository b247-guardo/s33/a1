// Create a FETCH request using the GET method that will retrieve all the todo list items from JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {

    // Create an array using the map method to return just the title of every item and print the result in the console
    const titles = data.map(item => item.title);
    console.log(titles);
  })

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API
  fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(data => {
      const title = data.title;
      const status = data.completed ? 'completed' : 'not completed';

      //  Print a message in the console that will provide the title and status of the to do list item
      console.log(`Title: ${title}, Status: ${status}`);
    })

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API

    fetch('https://jsonplaceholder.typicode.com/todos',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              title: 'New Post',
              body: 'Hello World!',
              userId: 1
            })
        })
          .then(response => response.json())
          .then(data => console.log(data));

 // Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API

    fetch('https://jsonplaceholder.typicode.com/todos/1', 
      {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              id: 1,
              title: 'Lorem Ipsum',
              userId: 1
          })
      })
          .then(response => response.json())
          .then(data => console.log(data));

  // Update a to do list item by changing the data structure to contain the following properties:
          //Title
          //Description
          //Status
          //Date Completed
          //User ID

    fetch('https://jsonplaceholder.typicode.com/todos/1', {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              title: 'Dolor sit amet',
              description: 'Change the Lorem Ipsum to Dolor sit amet',
              status: true,
              dateCompleted: '2023-02-23T14:30:00.000Z',
              userId: 1
          })
    })
          .then(response => response.json())
          .then(data => console.log(data));

  // Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API

      fetch('https://jsonplaceholder.typicode.com/todos/2', {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: 'Nor is there anyone who loves or pursues or desires to obtain pain of itself, because its pain'
            })
      })
          .then(response => response.json())
          .then(data => console.log(data));

  // Update a to do list item by changing the status to complete and add a date when the status was changed

      fetch('https://jsonplaceholder.typicode.com/todos/2', {
            method: 'PATCH',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: 'Nor is there anyone who loves or pursues or desires to obtain pain of itself, because its pain',
                completed: true,
                dateCompleted: '2023-02-23T15:30:00.000Z'
            })
      })
            .then(response => response.json())
            .then(data => console.log(data));

  // Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API

      fetch('https://jsonplaceholder.typicode.com/todos/2', {
            method: 'DELETE',
      })
            .then(response => response.json())
            .then(data => console.log(data));
